



<article class="product-post">

    <section class="product-header">
        <!--        <div class="tabs"></div>-->
        <div class="down-arrow">Click to view product details</div>
        <?php
        //vars
        $hImage = get_field('header_image')
        ?>
        <picture>
            <source
                media="(min-width: 48.1em)"
                srcset="<?php echo $hImage['sizes']['large'] ?>"
            />
            <source
                media="(min-width: 640px)"
                srcset="<?php echo $hImage['sizes']['medium'] ?>, <?php echo $hImage['sizes']['large'] ?>"
            />
            <source
                srcset="<?php echo $hImage['sizes']['square'] ?>"
            />
            <img
                alt="<?php echo $hImage['alt'];?>"
                srcset="<?php echo $hImage['sizes']['medium'] ?>" />
        </picture>

        <?php
        $cat = get_the_category();
        // args
        $args = array(
            'post_type' => 'post',
            'posts_per_page'	=> -1,
            'orderby' => 'menu_order',
            'category_name' => 'products'
        );

        // query
        $query = new WP_Query( $args );
        $current_id = $query->get_queried_object_id();

        ?>

        <script type="application/javascript">
            var parentPostID = <?php global $post; echo $post->ID; ?>
        </script>

        <?php
        $categories = get_the_category();
        global $post;
        foreach( $categories as $category ){
            if( 0 != $category->parent )
                $child_cat = $category;
        }
        if( isset( $child_cat ) ){
        $args = array(
            'cat' => $child_cat->term_id,
            'orderby' => 'menu_order'
        );
        $related = new WP_Query( $args );
            if( $related->have_posts() ){
                echo '<ul class="tabs">';
                while( $related->have_posts() ){
                    $related->the_post();
//                    if( wp_get_post_parent_id( $post_ID ) == $related->post->ID){
//                        echo '<li class="active" id="">';
//                    } else {
//                        echo '<li>';
//                    }
                     echo '<li id="' . $post->ID . '"><a href="' . get_permalink() . '">';
                    the_title();
                    echo '</a></li>';
                }
                wp_reset_postdata();
                echo '</ul>';
            }
         }
    ?>

    </section>

    <section class="product-details-container" id="productDetails">

        <div class="product-title">
            <h1 class="main-title"><?php the_title(); ?></h1>
        </div>

        <div class="row">
            <div class="col product-col">
                <?php if( have_rows('details_list') ): ?>
                    <ul class="product-detail-list">
                        <?php while( have_rows('details_list') ): the_row();

                            // vars
                            $image = get_sub_field('image');
                            $copy = get_sub_field('text');
                            $rows = get_field('details_list');
                            $row_count = count($rows);
                            ?>
                            <li class="product-detail-list-item">
                                <div class="copy"><?php echo $copy ?></div>
                                <?php if( $image ): ?>
                                    <div class="image">
                                        <a href="<?php echo $image['sizes']['large'] ?>" class="img" title="<?php echo $image['alt'] ?>">
                                            <img src="<?php echo $image['sizes']['medium'] ?>" alt="<?php echo $image['alt'] ?>">
                                        </a>

                                    </div>
                                <?php endif; ?>
                            </li>
                        <?php endwhile; ?>

                    </ul>
                <?php endif; ?>
            </div>
            <div class="col product-col">
                <aside>
                    <table class="product-table">
                        <!--                        <thead>-->
                        <!--                        <tr>-->
                        <!--                            <th>Header Cell 1</th>-->
                        <!--                            <th>Header Cell 2</th>-->
                        <!--                        </tr>-->
                        <!--                        </thead>-->
                        <tbody>
                        <tr>
                            <td>MATERIAL</td>
                            <td><?php echo get_field('product_material'); ?></td>
                        </tr>
                        <tr>
                            <td>APPLICATIONS</td>
                            <td><?php echo get_field('product_applications'); ?></td>
                        </tr>
                        <tr>
                            <td>SHEET SIZES</td>
                            <td><?php echo get_field('product_sheet_sizes'); ?></td>
                        </tr>
                        <tr>
                            <td>THICKNESS</td>
                            <td><?php echo get_field('product_tickness'); ?></td>
                        </tr>
                        <tr>
                            <td>HYGIENE</td>
                            <td><?php echo get_field('product_hygene'); ?></td>
                        </tr>
                        <tr>
                            <td>CLEANING</td>
                            <td><?php echo get_field('product_cleaning'); ?></td>
                        </tr>
                        <tr>
                            <td>FINISH</td>
                            <td><?php echo get_field('product_finish'); ?></td>
                        </tr>
                        <tr>
                            <td>EDGE</td>
                            <td><?php echo get_field('product_edge'); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </aside>
                <aside>
                    <table class="product-resistance-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <div class="resistance-level">
                                        <span class="text">LOW</span><span class="arrow"></span><span class="text">HIGH</span>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                        <tbody>

                        <?php if( have_rows('product_resistance_items') ): ?>
                            <ul class="product-detail-list">
                                <?php while( have_rows('product_resistance_items') ): the_row();
                                    // vars
                                    $rows = get_field('product_resistance_items');
                                    $level = get_sub_field('level_status');
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    ?>
                                    <tr>
                                        <td><?php echo $title; ?></td>
                                        <td>
                                            <div class="level-container">
                                                <ul class="level-container--items" data-level="<?php echo $level ?>">
                                                    <li class="level-container-items--item"></li>
                                                    <li class="level-container-items--item"></li>
                                                    <li class="level-container-items--item"></li>
                                                    <li class="level-container-items--item"></li>
                                                    <li class="level-container-items--item"></li>
                                                </ul>
                                            </div>
                                            <div class="level-description">
                                                <?php echo $description; ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </aside>
            </div>
        </div>

        <div class="product-title">
            <h1>Decors</h1>
        </div>
        <?php if( have_rows('decors') ): ?>
            <ul class="decor-list">
                <?php while( have_rows('decors') ): the_row();

                    // vars
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $finish = get_sub_field('finish');

                    ?>
                    <li>
                        <a href="<?php echo $image['sizes']['medium'] ?>" class="group" title="<?php echo $title ?>">
                            <figure>
                                <img src="<?php echo $image['sizes']['square'] ?>" class="img-rounded" alt="<?php echo $image['alt']; ?>">
                                <figcaption><strong><?php echo $title ?></strong><br><?php echo $finish; ?></figcaption>
                            </figure>
                        </a>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </section>

    <section>

        <p>To order a sample of any of the decors that you can see to the right, give our sample hotline a call on 01738 494989 or email
            <a href="mailto:sales@shoresurfaces.co.uk">sales@shoresurfaces.co.uk</a></p>
        <p>We recommend that you see a sample before ordering any of our worktops, upstands & splashbacks.</p>
    </section>

</article>