<?php


function enqueue_styles() {
	$parent_style = 'parent-style';
	wp_enqueue_style( $parent_style , get_template_directory_uri().'/style.css' );

    wp_register_style( 'styles', get_stylesheet_directory_uri() . '/assets/shore-styles.css');
    wp_register_style( 'colorbox', get_stylesheet_directory_uri() . '/assets/css/colorbox.css');

    wp_enqueue_style('colorbox');
    wp_enqueue_style('styles');

}

function theme_add_javascripts() {
	//wp_deregister_script('jquery');
    wp_register_script( 'easing', get_stylesheet_directory_uri() . '/assets/js/jquery.easing.1.3.js', true, null, false);
	wp_register_script( 'jqueryShore', 'https://code.jquery.com/jquery-2.1.1.min.js', '', '2.1.1', false);
    wp_register_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', '', '', true );
    wp_register_script( 'picturefill', get_stylesheet_directory_uri() . '/assets/js/picturefill.min.js', '', '', false );
    wp_register_script( 'matchmedia', get_stylesheet_directory_uri() . '/assets/js/matchMedia.js', array('jquery'), null, true );
    wp_register_script( 'colorbox', get_stylesheet_directory_uri() . '/assets/js/jquery.colorbox-min.js', array('jquery'), null, true );
    wp_register_script( 'scripts', get_stylesheet_directory_uri() . '/assets/js/shore_scripts.js?ver=1.1', array('jquery'), null, true );
    wp_register_script( 'cycle2', get_stylesheet_directory_uri() . '/assets/js/jquery.cycle2.min.js', array('jquery'), null, true );
    wp_register_script( 'cycle2-caption', get_stylesheet_directory_uri() . '/assets/js/jquery.cycle2.caption2.min.js', array('jquery'), null, true );
    wp_register_script( 'flexibility', get_stylesheet_directory_uri() . '/assets/js/flexibility.js', array('jquery'), null, true );
    wp_register_script( 'slideout', get_stylesheet_directory_uri() . '/assets/js/slideout.min.js', array('jquery'), null, false );
    wp_register_script( 'mobile', get_stylesheet_directory_uri() . '/assets/js/jquery.mobile.custom.min.js', array('jquery'), null, true );
    wp_register_script( 'map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA45JdwWh3PLtQwvEEtCDTeyB-u02oLNVE', array('jquery'), null, true );



    wp_enqueue_script('jqueryShore');
    wp_enqueue_script('easing');
    //wp_enqueue_script('slideout');
    wp_enqueue_script('bootstrap');
	wp_enqueue_script('picturefill');
    wp_enqueue_script('matchmedia');
    wp_enqueue_script('colorbox');
    wp_enqueue_script('cycle2');
    wp_enqueue_script('cycle2-caption');
    wp_enqueue_script('mobile');

    if ( is_page( 'contact' ) ) {
        wp_enqueue_script('map');
    }

    wp_enqueue_script('scripts');
}

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {

    }
    show_admin_bar(false);
}


function disable_wp_emojicons() {

    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

add_action( 'init', 'disable_wp_emojicons' );
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'theme_add_javascripts' );
add_action('after_setup_theme', 'remove_admin_bar');


remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action ('wp_head', 'rsd_link');


// cropped hero
add_image_size( 'mytheme-hero-cropped', 767, 460, true );

// scaled-down cropped hero 1
add_image_size( 'mytheme-hero-cropped-smaller', 560, 336, true );

// scaled-down cropped hero 2
add_image_size( 'mytheme-hero-cropped-smallest', 360, 216, true );


add_filter( 'wp_calculate_image_srcset',
	'mytheme_remove_images_below_breakpoint', 10, 5 );


// Defines image sizes
add_image_size('square'     , 500, 500, true);
add_image_size('preloader' , 16);
add_image_size('small'     , 600, 600, true);
add_image_size('medium'    , 1200, 600, true);
add_image_size('large'     , 1920, 900, true);

$defaults = array(
    'default-image'          => '',
    'width'                  => 0,
    'height'                 => 0,
    'flex-height'            => false,
    'flex-width'             => false,
    'uploads'                => true,
    'random-default'         => false,
    'header-text'            => true,
    'default-text-color'     => '',
    'wp-head-callback'       => '',
    'admin-head-callback'    => '',
    'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );

function shore_menu() {
    register_nav_menu('shore-menu',__( 'Shore Menu' ));
}
add_action( 'init', 'shore_menu' );

function top_menu() {
    register_nav_menu('top-menu',__( 'Top Menu' ));
}
add_action( 'init', 'top_menu' );

function footer_menu() {
    register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'footer_menu' );

// Allows us to get a base64 encoded image
function base64Encoder($url)
{
	$image = file_get_contents($url);
	return base64_encode($image);
}

function my_acf_google_map_api( $api ){

    $api['key'] = 'AIzaSyA45JdwWh3PLtQwvEEtCDTeyB-u02oLNVE';

    return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');