

var slideOut = {

    init: function() {

        slideOut.makeSlideout();
    },

    makeSlideout: function () {
        var slideout = new Slideout({
            'panel': document.getElementById('site-wrapper'),
            'menu': document.getElementById('mobile-menu'),
            'padding': 256,
            'tolerance': 70
        });
        // Toggle button
        document.querySelector('.toggle-button').addEventListener('click', function() {
            slideout.toggle();
        });
    }


}

slideOut.init();