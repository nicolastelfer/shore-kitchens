<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="" />
    <title><?php bloginfo('title'); ?></title>

    <?php wp_head(); ?>

    <!-- InternetExplorer-->
    <!--[if lte IE 9]>
    <link href="<?php bloginfo('template_directory');?>/assets/ie-styles.css" rel="stylesheet">
    <![endif]-->



    <!--[if lte IE 8]>
    <div class="ie-banner" style="display: none">
        <div class="content">
            <h1>We have noticed you still use a very old browser!</h1>
            <p>Unfortunately this site is not optimised for your browser. We recommend that you update your browser now to enjoy the best features modern browsers provide, starting with a more secure browsing!</p>
            <p><strong>Get a newer browser here:</strong></p> <p><a href="https://www.google.com/chrome/">Google Chrome <em>(Recommended)</em></a> or <a href="https://www.microsoft.com/en-gb/download/internet-explorer.aspx">Internet Explorer</a> </p>
        </div>

    </div>
    <![endif]-->

    <!--[if gt IE 9]>

    <![endif]-->
    
    
    <!-- Add google analytics -->

</head>

<body>

<div id="mobile-menu" class="mobile-menu slideout-menu slideout-menu-left">

    <?php if ( has_nav_menu( 'top-menu' ) ) : ?>
        <nav class="navigation top-navigation navigation-mobile" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'shore' ); ?>">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'top-menu',
                'menu_class'     => 'top-menu',
            ) );
            ?>
        </nav><!-- .main-navigation -->
    <?php endif; ?>
    <?php if ( has_nav_menu( 'shore-menu' ) || has_nav_menu( 'social' ) ) : ?>

        <div id="site-header-menu" class="site-header-menu">
            <?php if ( has_nav_menu( 'shore-menu' ) ) : ?>
                <nav id="site-navigation" class="navigation main-navigation navigation-mobile" role="navigation" aria-label="<?php esc_attr_e( 'Shore Menu', 'shore' ); ?>">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'shore-menu',
                        'menu_class'     => 'shore-menu',
                    ) );
                    ?>
                </nav><!-- .main-navigation -->
            <?php endif; ?>

            <?php if ( has_nav_menu( 'social' ) ) : ?>
                <nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'social',
                        'menu_class'     => 'social-links-menu',
                        'depth'          => 1,
                        'link_before'    => '<span class="screen-reader-text">',
                        'link_after'     => '</span>',
                    ) );
                    ?>
                </nav><!-- .social-navigation -->
            <?php endif; ?>
        </div><!-- .site-header-menu -->
    <?php endif; ?>
</div>
<div id="site-wrapper">
    <button class="toggle-button">☰</button>
    <header id="masthead" class="site-header" role="banner">
        <div class="site-header-main">
            <?php if ( has_nav_menu( 'top-menu' ) ) : ?>
                <nav class="navigation top-navigation" style="display: none;" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'shore' ); ?>">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'top-menu',
                        'menu_class'     => 'top-menu',
                    ) );
                    ?>
                </nav><!-- .main-navigation -->
            <?php endif; ?>
            <?php if ( has_nav_menu( 'shore-menu' ) || has_nav_menu( 'social' ) ) : ?>

                <div id="site-header-menu" class="site-header-menu">
                    <?php if ( has_nav_menu( 'shore-menu' ) ) : ?>
                        <nav id="site-navigation" class="navigation main-navigation " style="display: none;" role="navigation" aria-label="<?php esc_attr_e( 'Shore Menu', 'shore' ); ?>">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'shore-menu',
                                'menu_class'     => 'shore-menu',
                            ) );
                            ?>
                        </nav><!-- .main-navigation -->
                    <?php endif; ?>

                    <?php if ( has_nav_menu( 'social' ) ) : ?>
                        <nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'social',
                                'menu_class'     => 'social-links-menu',
                                'depth'          => 1,
                                'link_before'    => '<span class="screen-reader-text">',
                                'link_after'     => '</span>',
                            ) );
                            ?>
                        </nav><!-- .social-navigation -->
                    <?php endif; ?>
                </div><!-- .site-header-menu -->
            <?php endif; ?>
        </div><!-- .site-header-main -->

        <?php if ( get_header_image() ) : ?>
            <?php
            /**
             * Filter the default twentysixteen custom header sizes attribute.
             *
             * @since Twenty Sixteen 1.0
             *
             * @param string $custom_header_sizes sizes attribute
             * for Custom Header. Default '(max-width: 709px) 85vw,
             * (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px'.
             */
             $custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
            ?>
            <div class="header-image">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

                    <img src="<?php bloginfo('template_directory');?>/assets/images/shore-logo.svg" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                </a>
            </div><!-- .header-image -->
        <?php endif; // End header image check. ?>
    </header><!-- .site-header -->



    <main id="main" class="site-main" role="main">
        <a class="skip-link screen-reader-text" href="#content">Skip to Content</a>
