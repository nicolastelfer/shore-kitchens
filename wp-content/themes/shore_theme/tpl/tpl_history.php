<?php /* Template Name: History Template */ ?>

<?php get_header(); ?>

<section class="cd-horizontal-timeline">
    <div class="timeline">
        <div class="events-wrapper">
            <div class="events">
                <?php if( have_rows('history_slides') ): ?>
                    <ol>
                        <?php while( have_rows('history_slides') ): the_row();
                        // vars
                        $image = get_sub_field('image');
                        $date = get_sub_field('date');
                        $text = get_sub_field('text');
                        ?>
                        <li><a href="#0" data-date="01/01/<?php echo $date; ?>"><?php echo $date; ?></a></li>
                        <?php endwhile; ?>
                    </ol>

                <?php endif; ?>
                <span class="filling-line" aria-hidden="true"></span>
            </div> <!-- .events -->
        </div> <!-- .events-wrapper -->

        <ul class="cd-timeline-navigation">
            <li><a href="#0" class="prev inactive">Prev</a></li>
            <li><a href="#0" class="next">Next</a></li>
        </ul> <!-- .cd-timeline-navigation -->
    </div> <!-- .timeline -->

    <div class="events-content">
        <div class="carousel-pager">
            <div class="carousel-pager-direction">
                <a href="" class="cycle-next">Next</a><a href="" class="cycle-prev">Prev</a>
            </div>
        </div>
        <?php if( have_rows('history_slides') ): ?>
            <ol>
                <?php while( have_rows('history_slides') ): the_row();
                    // vars
                    $image = get_sub_field('image');
                    $date = get_sub_field('date');
                    $text = get_sub_field('text');
                    ?>
                    <li data-date="01/01/<?php echo $date; ?>">

                        <div class="timeline-image-wrapper">
                            <?php if( $image ): ?>
                                <picture>
                                    <source
                                        media="(min-width: 1240px)"
                                        srcset="<?php echo $image['sizes']['large'] ?>"
                                    />
                                    <source
                                        media="(min-width: 48.1em)"
                                        srcset="<?php echo $image['sizes']['medium'] ?>"
                                    />
                                    <source
                                        srcset="<?php echo $image['sizes']['square'] ?>"
                                    />
                                    <img
                                        alt="<?php echo $image['alt'];?>"
                                        srcset="<?php echo $image['sizes']['medium'] ?>" />
                                </picture>
                            <?php endif; ?>
                            <div class="timeline-image-details">
                                <strong><?php echo $date; ?></strong><br>
                                <?php echo $text; ?>
                            </div>

                        </div>

                    </li>
                <?php endwhile; ?>
            </ol>

        <?php endif; ?>

    </div> <!-- .events-content -->
</section>

<?php get_footer(); ?>
