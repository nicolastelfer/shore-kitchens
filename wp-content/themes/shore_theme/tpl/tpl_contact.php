<?php /* Template Name: Contact Template */ ?>

<?php get_header(); ?>



<article class="contact">

    <section class="map">
        <?php

        $location = get_field('map');

        if( !empty($location) ):
            ?>

            <script type="text/javascript">
                var templateUrl = '<?php echo get_bloginfo("template_url"); ?>';
            </script>
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
        <?php endif; ?>
    </section>

    <ul class="contact-details">

        <li class="contact-details-item contact-details-item--address">

            <div class="contact-address-col">
                <address>
                    <?php echo get_field('address') ?>
                </address>
            </div>
            <div class="contact-address-col">
                <address>
                    Tel: <?php echo get_field('phone') ?> <br>
                    <?php if (get_field('fax')): ?>
                    Fax: <?php echo get_field('fax') ?> <br>
                    <?php endif; ?>
                    <a href="<?php echo get_field('url') ?>"><?php echo get_field('url_title') ?></a> <br>
                    <a href="mailto:<?php echo get_field('email') ?>"></a><?php echo get_field('email') ?>
                </address>
            </div>
            <div class="contact-address-col">
                <ul class="social-links">
                    <li><a href="" class="twitter"><i></i>span@shoresurfaces</a></li>
                    <li><a href="" class="facebook"><i></i>shoresurfaces</a></li>
                </ul>
            </div>

        </li>
        <li class="contact-details-item">
            <?php echo get_field('sample_hotline') ?>
        </li>
        <li class="contact-details-item">
            <?php echo get_field('helpline') ?>
        </li>

    </ul>

</article>


<?php get_footer(); ?>
