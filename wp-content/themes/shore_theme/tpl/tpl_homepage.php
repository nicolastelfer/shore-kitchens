<?php /* Template Name: Home Template */ ?>

<?php get_header(); ?>

<?php if( have_rows('slides') ): ?>

    <ul class="cycle-slideshow cycle-main"
        data-cycle-fx="scrollHorz"
        data-cycle-caption-plugin=caption2
        data-cycle-caption-fx-sel=undefined
        data-cycle-overlay-fx-out="fadeOut"
        data-cycle-overlay-fx-in="fadeIn"
        data-cycle-speed="1000"
        data-cycle-timeout="3000"
        data-cyle-ease="easeInOutSine"
        data-cycle-loader="true"
        data-cycle-overlay-template="{{title}}<p><a href='{{desc}}'>Read More</a></p>"
    >
        <div class="carousel-pager">
            <div class="carousel-pager-direction">
                <a href="" class="cycle-next">Next</a><a href="" class="cycle-prev">Prev</a>
            </div>
        </div>
        <?php while( have_rows('slides') ): the_row();
            // vars
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $colour = get_sub_field('title_colour');
            $text = get_sub_field('text');
            $link = get_sub_field('link_url');
            ?>

            <div class="cycle-overlay slide-text-wrapper"></div>
            <li class="slide-item" data-cycle-title="<?php echo $text; ?>" data-cycle-desc="<?php echo $link; ?>">
                <div class="slide-title"  style="color: <?php echo $colour; ?>;"><?php echo $title; ?></div>
                <?php if( $image ): ?>
                    <?php if( $link ): ?>
                        <a href="<?php echo $link; ?>">
                    <?php endif; ?>
                    <picture>
                        <source
                            media="(min-width: 1240px)"
                            srcset="<?php echo $image['sizes']['large'] ?>"
                        />
                        <source
                            media="(min-width: 48.1em)"
                            srcset="<?php echo $image['sizes']['medium'] ?>"
                        />
                        <source
                            srcset="<?php echo $image['sizes']['square'] ?>"
                        />
                        <img
                            alt="<?php echo $image['alt'];?>"
                            srcset="<?php echo $image['sizes']['medium'] ?>" />
                    </picture>
                    <?php if( $link ): ?>
                        </a>
                    <?php endif; ?>
                <?php endif; ?>
            </li>


         <?php endwhile; ?>
    </ul>
<?php endif; ?>
<?php if( have_rows('slides') ): ?>
    <ul class="cycle-slideshow" data-cycle-fx="fadeout" data-cycle-speed="500" data-cycle-timeout="3500" >
        <?php while( have_rows('slides') ): the_row();
            $text = get_sub_field('text');
            $link = get_sub_field('link_url');
            ?>
            <li class="slide-item">
                <?php if( $text ): ?>
                    <div class="slide-text-wrapper slide-text-wrapper-mobile">
                        <?php echo $text; ?>
                        <p><a href="<?php echo $link; ?>">Read More</a></p>
                    </div>
                <?php endif; ?>
            </li>
        <?php endwhile; ?>
    </ul>
<?php endif; ?>

<?php get_footer(); ?>
