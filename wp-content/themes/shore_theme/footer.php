<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 */
?>

<!--Contact -->
<footer>
	<div class="footer-main">
		<div class="col col-footer-navigation">
			<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
				<nav id="footer-navigation" class="footer-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer  Links Menu', 'shore' ); ?>">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'footer-menu',
						'menu_class'     => 'footer-menu',
					) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>
		</div>
		<div class="col">
			<div class="copyright">&copy; <?php bloginfo( 'name' ); ?> <?php echo date('Y') ?> | <a href="mailto:sales@shoresurfaces.co.uk">sales@shoresurfaces.co.uk</a></div>
		</div>


	</div>

</footer>

</main><!-- .site-main -->

</div><!-- .site-wrapper -->


<!--[if gte IE 8]>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/respond.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/matchMedia.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/matchMedia.addListener.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/matchMedia.polyfill.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/flexibility.js"></script>
<![endif]-->

<script type="text/javascript"src="<?php bloginfo('template_directory');?>/assets/js/slideout.min.js"></script>
<script type="text/javascript"src="<?php bloginfo('template_directory');?>/assets/js/shore_slideout_script.js"></script>

<?php wp_footer(); ?>



</body>
</html>
